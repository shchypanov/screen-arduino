import React, { Fragment } from 'react';
import Hero from './Hero';
import HomeContent from './HomeContent';

export default function Home() {
  return (
    <Fragment>
      <Hero />
      <div className="box cta">
        <h2>Welcome! It's a test Arduino IoT Project. Go to Admin page and enter yout first string.</h2>
      </div>
      <HomeContent />
    </Fragment>
  )
}
