import React, { Component, Fragment } from 'react';
import String from './String';
import axios from "axios";
const config = require('../config.json');

export default class StringAdmin extends Component {

  state = {
    newstring: {
      "stringname": "",
      "id": ""
    },
    strings: []
  }

  handleAddString = async (id, event) => {
    event.preventDefault();
    try {
      const params = {
        "id": id,
        "stringname": this.state.newstring.stringname
      };
      await axios.post(`${config.api.invokeUrl}/strings/${id}`, params);
      this.setState({ strings: [...this.state.strings, this.state.newstring] });
      this.setState({ newstring: { "stringname": "", "id": "" }});
    }catch (err) {
      console.log(`An error has occurred: ${err}`);
    }
  }

  handleUpdateString = async (id, name) => {
    try {
      const params = {
        "id": id,
        "stringname": name
      };
      await axios.patch(`${config.api.invokeUrl}/strings/${id}`, params);
      const stringToUpdate = [...this.state.strings].find(string => string.id === id);
      const updatedStrings = [...this.state.strings].filter(string => string.id !== id);
      stringToUpdate.stringname = name;
      updatedStrings.push(stringToUpdate);
      this.setState({strings: updatedStrings});
    }catch (err) {
      console.log(`Error updating string: ${err}`);
    }
  }

  handleDeleteString = async (id, event) => {
    event.preventDefault();
    try {
      await axios.delete(`${config.api.invokeUrl}/strings/${id}`);
      const updatedStrings = [...this.state.strings].filter(string => string.id !== id);
      this.setState({strings: updatedStrings});
    }catch (err) {
      console.log(`Unable to delete string: ${err}`);
    }
  }

  fetchStrings = async () => {
    try {
      const res = await axios.get(`${config.api.invokeUrl}/strings`);
      const strings = res.data;
      this.setState({ strings: strings });
    } catch (err) {
      console.log(`An error has occurred: ${err}`);
    }
  }

  onAddStringNameChange = event => this.setState({ newstring: { ...this.state.newstring, "stringname": event.target.value } });
  onAddStringIdChange = event => this.setState({ newstring: { ...this.state.newstring, "id": event.target.value } });

  componentDidMount = () => {
    this.fetchStrings();
  }

  render() {
    return (
      <Fragment>
        <section className="section">
          {this.props.auth.isAuthenticated && (
            <div className="container">
              <h1>You are an Admin! :)</h1>
              <p className="subtitle is-5">It's cool! You can add a new string and the screen text will change!</p>
              <p className="subtitle is-5">Also a new value is added to the database.</p>
              <p className="subtitle is-5">You can even change it or remove.</p>
              <br />
              <div className="columns">
                <div className="column is-one-third">
                  <form onSubmit={event => this.handleAddString(this.state.newstring.id, event)}>
                    <div className="field has-addons">
                      <div className="control">
                        <input
                          className="input is-medium"
                          type="text"
                          placeholder="String"
                          value={this.state.newstring.stringname}
                          onChange={this.onAddStringNameChange}
                        />
                      </div>
                      <div className="control">
                        <input
                          className="input is-medium"
                          type="text"
                          placeholder="ID"
                          value={this.state.newstring.id}
                          onChange={this.onAddStringIdChange}
                        />
                      </div>
                      <div className="control">
                        <button type="submit" className="button is-primary is-medium">
                          Change string now!
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
                <div className="column is-two-thirds">
                  <div className="tile is-ancestor">
                    <div className="tile is-4 is-parent  is-vertical">
                      {
                        this.state.strings.map((string, index) =>
                          <String
                            isAdmin={true}
                            handleUpdateString={this.handleUpdateString}
                            handleDeleteString={this.handleDeleteString}
                            name={string.stringname}
                            id={string.id}
                            key={string.id}
                          />)
                      }
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
          {!this.props.auth.isAuthenticated && (
            <p className="subtitle is-5">You should be an admin! Please Log In or Register.</p>
          )}
        </section>
      </Fragment>
    )
  }
}
