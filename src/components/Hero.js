import React from 'react';

export default function Hero() {
  return (
    <section className="is-primary">
      <div className="hero-body">
        <div className="container">
          <img src="arduino.jpg" alt="conserve energy" />
        </div>
      </div>
    </section>
  )
}
