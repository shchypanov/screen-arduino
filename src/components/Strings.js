import React, { Component, Fragment } from 'react';
import String from './String';
import axios from "axios";
const config = require('../config.json');

export default class Strings extends Component {

  state = {
    newstring: null,
    strings: []
  }

  fetchStrings = async () => {
    try {
      const res = await axios.get(`${config.api.invokeUrl}/strings`);
      const strings = res.data;
      this.setState({ strings: strings });
    } catch (err) {
      console.log(`An error has occurred: ${err}`);
    }
  }

  componentDidMount = () => {
    this.fetchStrings();
  }

  render() {
    return (
      <Fragment>
        <section className="section">
          <div className="container">
            <h1>All Strings</h1>
            <p className="subtitle is-5">This is a list of all strings that have ever been added:</p>
            <br />
            <div className="columns">
              <div className="column">
                <div className="tile is-ancestor">
                  <div className="tile is-4 is-parent  is-vertical">
                    {
                      this.state.strings && this.state.strings.length > 0
                      ? this.state.strings.map(string => <String name={string.stringname} id={string.id} key={string.id} />)
                      : <div className="tile notification is-warning">No strings available</div>
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </Fragment>
    )
  }
}
