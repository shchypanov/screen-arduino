import React, { Component, Fragment }  from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default class StringAdmin extends Component {

  state = {
    isEditMode: false,
    updatedstringname: this.props.name
  }

  handleStringEdit = event => {
    event.preventDefault();
    this.setState({ isEditMode: true });
  }

  handleEditSave = event => {
    event.preventDefault();
    this.setState({ isEditMode: false });
    this.props.handleUpdateString(this.props.id, this.state.updatedstringname);
  }

  onAddStringNameChange = event => this.setState({ "updatedstringname": event.target.value });

  render() {
    return (
      <div className="tile is-child box notification is-success">
        {
          this.props.isAdmin &&
          <Fragment>
            <a href="/" onClick={this.handleStringEdit} className="string-edit-icon">
              <FontAwesomeIcon icon="edit" />
            </a>
            <button onClick={event => this.props.handleDeleteString(this.props.id, event)} className="delete"></button>
          </Fragment>
        }
        {
          this.state.isEditMode
          ? <div>
              <p>Edit string name</p>
              <input
                className="input is-medium"
                type="text"
                placeholder="Enter name"
                value={this.state.updatedstringname}
                onChange={this.onAddStringNameChange}
              />
              <p className="string-id">id: { this.props.id }</p>
              <button type="submit"
                className="button is-info is-small"
                onClick={ this.handleEditSave }
              >save</button>
            </div>
          : <div>
              <p className="string-title">{ this.props.name }</p>
              <p className="string-id">id: { this.props.id }</p>
            </div>
        }
      </div>
    )
  }
}
